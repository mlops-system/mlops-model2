FROM python:3-slim
LABEL maintainer="Cleber Castro <cjj.castro3@gmail.com>"

WORKDIR /app/

RUN apt-get update -y && \
  apt-get install -y libpq-dev python3-dev gcc

COPY requirements.txt .
COPY model.py .

RUN pip install --no-cache-dir -r requirements.txt && \
  rm requirements.txt

CMD python model.py 0.01 0.01